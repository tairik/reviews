        <?php
        //ini_set('display_errors', 1);
        //error_reporting(E_ALL);
        include 'library/CallAPI.php';
        include 'library/Client.php';

        $objCallAPI = new Library\CallAPI();
        $objCallAPI->setOffset($_GET['offset']);


        $filter_source = explode(',',substr($_GET['filter_source'], 1));

        $google = false;
        if (in_array('google', $filter_source)) {
            $objCallAPI->setGoogle(1);
            $google = true;
        }

        $yelp = false;
        if (in_array('yelp', $filter_source)) {
            $objCallAPI->setYelp(1);
            $yelp = true;
        }

        $internal = false;
        if (in_array('internal', $filter_source)) {
            $objCallAPI->setInternal(1);
            $internal = true;
        }

        $return      = $objCallAPI->call();
        $objClient   = new Library\Client($return);
        $reviews     = $objClient->getReviews();
        $aryColors   = array();
        $aryColors[] = 'success';
        $aryColors[] = 'info';
        $aryColors[] = 'danger';
        $aryColors[] = 'warning';
        ?>
        <div class="row text-center">
            <div class="col-md-12">
                <h2><?php echo $objClient->getBusinessName();?></h2>
                <span><?php echo $objClient->getBusinessAddress();?></span>
                <span><?php echo $objClient->getBusinessPhone();?></span>
                <br /><br />
            </div>
        </div>

        <div class="row">
            <div class="col-md-6"><span id="total-records" data-total="<?php echo $objClient->getTotalNoOfReviews();?>">Number of reviews: <?php echo $objClient->getTotalNoOfReviews();?></span></div>
        </div>
        <br />

        <div class="row">
            <div class="col-md-6"><span>Filters Review Source: <label for="filter_source_google">Google</labeL> <input type="checkbox" <?php if ($google) { ?> checked <?php } ?> class="filter_source" name="filter_source" value="google" id="filter_source_google"> <label for="filter_source_yelp">Yelp</labeL> <input <?php if ($yelp) { ?> checked <?php } ?>  class="filter_source" type="checkbox" name="filter_source" value="yelp" id="filter_source_yelp"> <label for="filter_source_internal">Internal</labeL> <input <?php if ($internal) { ?> checked <?php } ?>  class="filter_source" type="checkbox" name="filter_source" value="internal" id="filter_source_internal"></span></div>
        </div>

        <br/>
        <div class="row">
        <?php foreach ($reviews as $objReview) { ?>
            <div class="col-md-4">
               <div class="alert alert-<?php echo $aryColors[rand(0,3)] ?>" >
                   <?php echo $objReview->getDescription(); ?>
                    <h5 class="get-right"><strong> - <?php echo $objReview->getCustumerName(); ?>  </strong> - Source - <?php echo $objReview->getReviewSource(); ?></h5>
               </div>
            </div>
        <?php } ?>
        </div>