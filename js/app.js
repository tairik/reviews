jQuery.fn.extend({
    fetchHTML: function (offset, limit) {

      var checked = '';
      $('.filter_source:checked').each(function(){
          checked = checked + ',' + $(this).val();
      });
    $.get( "partial.php?offset="+offset+"&limit="+limit+"&filter_source="+checked, function( data ) {
      $( "#reviews" ).html( data );
    });
    }
});

$( "#previous" ).click(function() {
  var aryOffsetLimit = recalculateOffset('-');
  $('.reviews').fetchHTML(aryOffsetLimit['offset'], aryOffsetLimit['limit']);
});

$( "#next" ).click(function() {
  var total  = $('#total-records').data('total');
  var offset = $('#previous').data('offset');
  var limit  = $('#previous').data('limit');
  var aryOffsetLimit = recalculateOffset('+');
  $('.reviews').fetchHTML(aryOffsetLimit['offset'], aryOffsetLimit['limit']);
});

$(document).on('click', ".filter_source", function () {
  var offset = 0;
  var limit  = $('#previous').data('offset', 0);  
  var limit  = $('#previous').data('limit');  
  $('.reviews').fetchHTML(offset, limit);
});

function recalculateOffset (operation) {

    var offset    = $('#previous').data('offset');
    var limit     = $('#previous').data('limit');
    var newOffset = eval(offset + operation + "6");
    if (newOffset <= 0) {
      newOffset = 0;
    }
    var aryOffsetLimit       = Array();
    aryOffsetLimit['offset'] = newOffset;
    aryOffsetLimit['limit']  = limit;
  $('#previous').data('offset', newOffset);

  return aryOffsetLimit;
}