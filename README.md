# Build a simple PHP application page with Reviews

Write a simple one-page application using PHP, HTML, CSS, and Javascript that consumes the Reputation Loop API and renders a page displaying the company’s details along with a list of reviews.

**Required endpoints**

1. API endpoint

  http://test.localfeedbackloop.com/api?apiKey=61067f81f8cf7e4a1f673cd230216112&noOfReviews=10&internal=1&yelp=1&google=1&offset=50&threshold=1

2. Install

  Add the follow line in your local host file: 104.131.162.0  local.reputation-loop.com

3. Open your web browser with the follow URL:

  http://local.reputation-loop.com/

## Things To Consider in the future

- This is a very simple page where we display the reviews for a specific customer with the data coming from a REST Service. No framework was used, no ORM, no MVC or other design pattern - as requested "one-page".
- It seems the API has a bug with the filters
- Handle the errors
- Sanitize the HTTP request
- Object data model
- Cache the API calls - Memcache
- Pagination can be improved with number of pages
- 