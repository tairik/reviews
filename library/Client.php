<?php

namespace Library;
include 'library/Review.php';

class Client {

	/**
     * @var business_name
     */
	public $business_name;

	/**
     * @var business_address
     */
	public $business_address;

	/**
     * @var business_phone
     */
	public $business_phone;

	/**
     * @var external_url
     */
	public $external_url;

	/**
     * @var external_page_url
     */
	public $external_page_url;

	/**
     * @var total_avg_rating
     */
	public $total_avg_rating;

	/**
     * @var total_no_of_reviews
     */
	public $total_no_of_reviews;

	/**
     * @var reviews
     */
	public $reviews;


	/**
     * Set the object with initial values
     *
     * @param array $infos
     */
	public function __construct($infos)
	{
		$valuesDecoded = json_decode($infos);
		if ($valuesDecoded) {
			if ($valuesDecoded->business_info) {
				$this->setBusinessName($valuesDecoded->business_info->business_name);
			}
			if ($valuesDecoded->business_info->business_address) {
				$this->setBusinessAddress($valuesDecoded->business_info->business_address);
			}
			if ($valuesDecoded->business_info->business_phone) {
				$this->setBusinessPhone($valuesDecoded->business_info->business_phone);
			}
			if ($valuesDecoded->business_info->total_rating->total_avg_rating) {
				$this->setTotalAvgRating($valuesDecoded->business_info->total_rating->total_avg_rating);
			}
			if ($valuesDecoded->business_info->total_rating->total_no_of_reviews) {
				$this->setTotalNoOfReviews($valuesDecoded->business_info->total_rating->total_no_of_reviews);
			}
			if ($valuesDecoded->business_info->external_url) {
				$this->setExternalURL($valuesDecoded->business_info->external_url);
			}
			if ($valuesDecoded->business_info->external_page_url) {
				$this->setExternalPageURL($valuesDecoded->business_info->external_page_url);
			}
		}

		$this->setReviews($valuesDecoded);
	}

	/**
     * Set the business_name
     *
     * @param string $business_name
     */
	public function setBusinessName ($business_name)
	{
		$this->business_name = $business_name;
	}

	/**
     * Set the business_address
     *
     * @param string $business_address
     */
	public function setBusinessAddress ($business_address)
	{
		$this->business_address = $business_address;
	}

	/**
     * Set the business_phone
     *
     * @param string $business_phone
     */
	public function setBusinessPhone ($business_phone)
	{
		$this->business_phone = $business_phone;
	}

	/**
     * Set the total_avg_rating
     *
     * @param string $total_avg_rating
     */
	public function setTotalAvgRating ($total_avg_rating)
	{
		$this->total_avg_rating = $total_avg_rating;
	}

	/**
     * Set the total_no_of_reviews
     *
     * @param string $total_no_of_reviews
     */
	public function setTotalNoOfReviews ($total_no_of_reviews)
	{
		$this->total_no_of_reviews = $total_no_of_reviews;
	}

	/**
     * Set the external_url
     *
     * @param string $external_url
     */
	public function setExternalURL ($external_url)
	{
		$this->external_url = $external_url;
	}

	/**
     * Set the external_page_url
     *
     * @param string $external_page_url
     */
	public function setExternalPageURL ($external_page_url)
	{
		$this->external_page_url = $external_page_url;
	}

	/**
     * Set the Reviews
     *
     * @param array $infos
     */
	public function setReviews($infos)
	{
		$aryReviews = array();
		foreach ($infos->reviews as $key => $value) {
			$aryReviews[] = new Review($value);
		}
		$this->reviews = $aryReviews;
	}

	/**
     * Get the business_name
     *
     */
	public function getBusinessName ()
	{
		return $this->business_name;
	}

	/**
     * Get the business_name
     *
     */
	public function getBusinessAddress ()
	{
		return $this->business_address;
	}

	/**
     * Get the business_phone
     *
     */
	public function getBusinessPhone ()
	{
		return $this->business_phone;
	}

	/**
     * Get the total_avg_rating
     *
     */
	public function getTotalAvgRating ()
	{
		return $this->total_avg_rating;
	}

	/**
     * Get the total_no_of_reviews
     *
     */
	public function getTotalNoOfReviews ()
	{
		return $this->total_no_of_reviews;
	}

	/**
     * Get the external_url
     *
     */
	public function getExternalURL ()
	{
		return $this->external_url;
	}

	/**
     * Get the external_page_url
     *
     */
	public function getExternalPageURL ()
	{
		return $this->external_page_url;
	}

	/**
     * Get the reviews
     *
     */
	public function getReviews ()
	{
		return $this->reviews;
	}

}