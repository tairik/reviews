<?php

namespace Library;

class CallAPI {

	/**
     * @var RequestInterface
     */
	private $url     = 'http://test.localfeedbackloop.com/api?apiKey=61067f81f8cf7e4a1f673cd230216112&noOfReviews=6&threshold=1';

	/**
     * @var offset
     */
	public $offset   = 0;

	/**
     * @var google
     */
	public $google   = 0;

	/**
     * @var yelp
     */
	public $yelp     = 0;

	/**
     * @var internal
     */
	public $internal = 0;

	/**
     * @var limit
     */
	public $limit    = 6;

	public function __construct()
	{
		return $this;
	}

	/**
     * Set the setOffSet
     *
     * @param int $offset
     */
	public function setOffSet($offset)
	{
		$this->offset = $offset;
	}

	/**
     * Set the setGoogle
     *
     * @param boolean $google
     */
	public function setGoogle($google)
	{
		$this->google = $google;
	}

	/**
     * Set the setYelp
     *
     * @param boolean $yelp
     */
	public function setYelp($yelp)
	{
		$this->yelp = $yelp;
	}

	/**
     * Set the setInternal
     *
     * @param boolean $internal
     */
	public function setInternal($internal)
	{
		$this->internal = $internal;
	}

	/**
     * Call
     */
	public function call ()
	{
		$this->url = $this->url . '&offset=' . $this->offset;
		$this->url = $this->url . '&google=' . $this->google;
		$this->url = $this->url . '&yelp=' . $this->yelp;
		$this->url = $this->url . '&internal=' . $this->internal;

		return file_get_contents($this->url);
	}
}