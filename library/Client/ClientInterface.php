<?php

namespace Rest\Client;

use Rest\InvoiceInterface;

/**
 * Sends request(s) to thw server
 *
 * @package Rest
 */
interface ClientInterface
{

    const NAME    = 'PHP-Client';
    const VERSION = '0.0.0';
}
