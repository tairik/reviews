<?php

namespace Rest\Client\Adapter;

use Rest\Client\RequestInterface;
use Rest\Client\ResponseInterface;

/**
 * A client can be configured to use a specific adapter to make requests, by
 * default the CurlAdapter is what is used.
 *
 * @package Rest
 */
interface AdapterInterface
{
    /**
     * Send request to Rest
     *
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     */
    public function sendRequest(RequestInterface $request);
}
