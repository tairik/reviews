<?php

namespace Rest\Client\Adapter;

use Rest\Client\RequestInterface;
use Rest\Client\ResponseInterface;
use Rest\Client\Response;

/**
 * Adapter that sends Request objects using CURL
 *
 * @TODO add way to configure curl with options
 *
 * @codeCoverageIgnore
 * @package Rest
 */
class CurlAdapter implements AdapterInterface
{
    /**
     * @inheritdoc
     */
    public function sendRequest(RequestInterface $request)
    {
        $curl = curl_init();
        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL            => $request->getUri(),
                CURLOPT_PORT           => 443,
                CURLOPT_HTTPHEADER     => $request->getHeaderFields(),
                CURLOPT_TIMEOUT        => 10,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_FORBID_REUSE   => 1,
                CURLOPT_FRESH_CONNECT  => 1,
                CURLOPT_HEADER         => true,
            )
        );

        if (RequestInterface::METHOD_POST == $request->getMethod()) {
            curl_setopt_array(
                $curl,
                array(
                    CURLOPT_POST           => 1,
                    CURLOPT_POSTFIELDS     => $request->getBody(),
                )
            );
        }

        /** @var ResponseInterface */
        $curl_response = curl_exec($curl);
        $response = Response::createFromRawResponse($curl_response);

        curl_close($curl);

        return $response;
    }
}
