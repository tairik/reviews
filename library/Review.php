<?php

namespace Library;

class Review {

	/**
     * @var customer_name
     */
	public $customer_name;

	/**
     * @var date_of_submission
     */
	public $date_of_submission;

	/**
     * @var customer_last_name
     */
	public $customer_last_name;

	/**
     * @var description
     */
	public $description;

	/**
     * @var rating
     */
	public $rating;

	/**
     * @var review_from
     */
	public $review_from;

	/**
     * @var review_url
     */
	public $review_url;

	/**
     * @var review_id
     */
	public $review_id;

	/**
     * @var customer_url
     */
	public $customer_url;

	/**
     * @var review_source
     */
	public $review_source;


	/**
     * Set the object with initial values
     *
     * @param array $infos
     */
	public function __construct($infos)
	{
		if ($infos->customer_name) {
			$this->setCustumerName($infos->customer_name);
		}
		if ($infos->date_of_submission) {
			$this->setDateOfSubmission($infos->date_of_submission);
		}
		if ($infos->customer_last_name) {
			$this->setCustumerLastName($infos->customer_last_name);
		}
		if ($infos->description) {
			$this->setDescription($infos->description);
		}
		if ($infos->rating) {
			$this->setRating($infos->rating);
		}
		if ($infos->review_from) {
			$this->setReviewFrom($infos->review_from);
		}
		if ($infos->review_url) {
			$this->setReviewUrl($infos->review_url);
		}
		if ($infos->review_id) {
			$this->setReviewId($infos->review_id);
		}
		if ($infos->customer_url) {
			$this->setCustomerUrl($infos->customer_url);
		}
		if ($infos->review_source) {
			$this->setReviewSource($infos->review_source);
		}
	}

	/**
     * Set the customer_name
     *
     * @param customer_name $customer_name
     */
	public function setCustumerName ($customer_name)
	{
		$this->customer_name = $customer_name;
	}

	/**
     * Set the date_of_submission
     *
     * @param string $date_of_submission
     */
	public function setDateOfSubmission ($date_of_submission)
	{
		$this->date_of_submission = $date_of_submission;
	}

	/**
     * Set the customer_last_name
     *
     * @param string $customer_last_name
     */
	public function setCustumerLastName ($customer_last_name)
	{
		$this->customer_last_name = $customer_last_name;
	}

	/**
     * Set the description
     *
     * @param string $description
     */
	public function setDescription ($description)
	{
		$this->description = $description;
	}

	/**
     * Set the rating
     *
     * @param string $rating
     */
	public function setRating ($rating)
	{
		$this->rating = $rating;
	}

	/**
     * Set the review_from
     *
     * @param string $review_from
     */
	public function setReviewFrom ($review_from)
	{
		$this->review_from = $review_from;
	}

	/**
     * Set the review_url
     *
     * @param string $review_url
     */
	public function setReviewUrl ($review_url)
	{
		$this->review_url = $review_url;
	}

	/**
     * Set the review_id
     *
     * @param string $review_id
     */
	public function setReviewId ($review_id)
	{
		$this->review_id = $review_id;
	}

	/**
     * Set the customer_url
     *
     * @param string $customer_url
     */
	public function setCustomerUrl ($customer_url)
	{
		$this->customer_url = $customer_url;
	}

	/**
     * Set the review_source
     *
     * @param string $review_source
     */
	public function setReviewSource ($review_source)
	{
		$this->review_source = $review_source;
	}

	/**
     * Get the customer_name
     *
     */
	public function getCustumerName ()
	{
		return $this->customer_name;
	}

	/**
     * Get the customer_name
     *
     */
	public function getDateOfSubmission ()
	{
		return $this->date_of_submission;
	}

	/**
     * Get the customer_name
     *
     */
	public function getCustumerLastName ()
	{
		return $this->customer_name;
	}

	/**
     * Get the description
     *
     */
	public function getDescription ()
	{
		return $this->description;
	}

	/**
     * Get the rating
     *
     */
	public function getRating ()
	{
		return $this->rating;
	}

	/**
     * Get the review_from
     *
     */
	public function getReviewFrom ()
	{
		return $this->review_from;
	}

	/**
     * Get the review_url
     *
     */
	public function getReviewUrl ()
	{
		$this->review_url;
	}

	/**
     * Get the review_id
     *
     */
	public function getReviewId ()
	{
		return $this->review_id;
	}

	/**
     * Get the customer_url
     *
     */
	public function getCustomerUrl ()
	{
		return $this->customer_url;
	}

	/**
     * Get the review_source
     *
     */
	public function getReviewSource ()
	{
		return $this->review_source;
	}
}